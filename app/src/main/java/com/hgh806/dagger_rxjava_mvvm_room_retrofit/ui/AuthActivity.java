package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui;

import android.os.Bundle;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.R;

import dagger.android.support.DaggerAppCompatActivity;

public class AuthActivity extends DaggerAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

    }

}
