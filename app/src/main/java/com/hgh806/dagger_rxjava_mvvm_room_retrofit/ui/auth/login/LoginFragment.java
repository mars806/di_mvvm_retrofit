package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.MainActivity;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.R;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.utils.ViewModelProviderFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

public class LoginFragment extends DaggerFragment {

    private static final String TAG = "LoginFragment";

    @BindView(R.id.email)
    EditText edtEmail;

    @BindView(R.id.password)
    EditText edtPassword;

    @BindView(R.id.btnVerify)
    Button btnVerify;

    @BindView(R.id.txtForgetPassword)
    TextView txtForgetPassword;

    @BindView(R.id.btn_register)
    LinearLayout btnRegister;

    @BindView(R.id.ln1)
    LinearLayout ln1;

    private ProgressDialog pDialog;

    private Unbinder unbinder;

    private LoginViewModel loginViewModel;

    @Inject
    public ViewModelProviderFactory providerFactory;


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        loginViewModel = new ViewModelProvider(this, providerFactory).get(LoginViewModel.class);

        subscribeObservers();

        FlowingGradientClass grad = new FlowingGradientClass();
        grad.setBackgroundResource(R.drawable.trans)
                .onLinearLayout(ln1)
                .setTransitionDuration(2000)
                .start();

        // Progress dialog
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);

    }

    private void subscribeObservers() {

        loginViewModel.observeAuthState().observe(getViewLifecycleOwner(), userAuthResource -> {
            if(userAuthResource != null){
                switch (userAuthResource.status){

                    case LOADING:{
                        showDialog();
                        break;
                    }

                    case AUTHENTICATED:{
                        hideDialog();
                        onLoginSuccess();
                        break;
                    }

                    case ERROR:{
                        hideDialog();
                        Toast.makeText(getContext(), userAuthResource.message
                                + "\nDid you enter a number between 1 and 10?", Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case NOT_AUTHENTICATED:{
                        hideDialog();
                        break;
                    }
                }
            }
        });
    }

    private void onLoginSuccess() {
        Toast.makeText(getContext(), "success", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @OnClick({R.id.btnVerify, R.id.txtForgetPassword, R.id.btn_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnVerify:
                attemptLogin();
                break;
            case R.id.txtForgetPassword:
                navigate(R.id.forgotPassword);
                break;
            case R.id.btn_register:
                navigate(R.id.register);
                break;
        }
    }


    private void attemptLogin() {
        if(TextUtils.isEmpty(edtEmail.getText().toString())
                || TextUtils.isEmpty(edtPassword.getText().toString())){
            return;
        }
        Log.d(TAG, "attemptLogin: email " + edtEmail.getText().toString() + " pass= " + edtPassword.getText().toString());
        loginViewModel.authenticateUser(edtEmail.getText().toString(), edtPassword.getText().toString());
    }

    private void navigate(int id){
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(id);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
