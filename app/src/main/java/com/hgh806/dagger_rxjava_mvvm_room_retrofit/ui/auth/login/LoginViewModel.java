package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.login;

import android.util.Log;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.SessionManager;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.database.AppDatabase;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.network.AuthApi;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.AuthResource;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.ViewModel;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends ViewModel {

    private static final String TAG = "LoginViewModel";

    private SessionManager sessionManager;
    private AuthApi authApi;
    private AppDatabase database;

    @Inject
    public LoginViewModel(AuthApi authApi, SessionManager sessionManager, AppDatabase database) {
        this.sessionManager = sessionManager;
        this.authApi = authApi;
        this.database = database;
    }

    public LiveData<AuthResource<UserInfoModel>> observeAuthState() {
        return sessionManager.getAuthUser();
    }

    public void authenticateUser(String email, String password) {
        Log.d(TAG, "attemptLogin: attempting to login.");
        sessionManager.authenticateWithId(queryUserId(email, password));
    }

    private LiveData<AuthResource<UserInfoModel>> queryUserId(String email, String password) {

        return LiveDataReactiveStreams.fromPublisher(authApi.login(email, password)

                // instead of calling onError, do this
                .onErrorReturn(throwable -> {
                    Log.i(TAG, "queryUserId: " + throwable);
                    UserInfoModel errorUser = new UserInfoModel();
                    errorUser.setStatus(-1);
                    return errorUser;
                })

                // wrap User object in AuthResource
                .map((Function<UserInfoModel, AuthResource<UserInfoModel>>) user -> {

                    if (user.getStatus() == -1) {
                        return AuthResource.error("Could not authenticate", null);
                    }
                    return AuthResource.authenticated(user);
                })
                .subscribeOn(Schedulers.io()));
    }


    public void saveToDatabase(UserInfoModel.User user) {

        database.userDAO().insertInfo(user);

    }
}
