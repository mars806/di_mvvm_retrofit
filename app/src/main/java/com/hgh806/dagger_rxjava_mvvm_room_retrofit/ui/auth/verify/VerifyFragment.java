package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.verify;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;


public class VerifyFragment extends DaggerFragment {

    @BindView(R.id.codeVerify)
    EditText codeVerify;
    @BindView(R.id.btnVerify)
    Button btnVerify;
    @BindView(R.id.ln1)
    LinearLayout ln1;

    private String code;
    private String email;

    private Unbinder unbinder;

    public VerifyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_verify, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        // Progress dialog
        ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Verifying in ...");

    }

    @OnClick(R.id.btnVerify)
    public void onViewClicked() {
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
