package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.forgotPassword;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.network.AuthApi;

import javax.inject.Inject;

import androidx.lifecycle.ViewModel;

public class ForgotPassViewModel extends ViewModel {

    AuthApi authApi;

    @Inject
    public ForgotPassViewModel(AuthApi authApi){
        this.authApi = authApi;
    }

    public void SendVerifyCode(){
        
    }
}
