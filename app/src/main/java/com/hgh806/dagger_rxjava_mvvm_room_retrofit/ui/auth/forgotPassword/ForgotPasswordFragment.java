package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.forgotPassword;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

public class ForgotPasswordFragment extends DaggerFragment {

    private ProgressDialog pDialog;

    @BindView(R.id.email)
    EditText username;
    @BindView(R.id.btnVerify)
    Button btnVerify;
    @BindView(R.id.btn_register)
    LinearLayout btnRegister;
    @BindView(R.id.ln1)
    LinearLayout ln1;

    private Unbinder unbinder;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        FlowingGradientClass grad = new FlowingGradientClass();
        grad.setBackgroundResource(R.drawable.trans)
                .onLinearLayout(ln1)
                .setTransitionDuration(2000)
                .start();

        // Progress dialog
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Verifying in ...");
    }

    public void checkLogin(View view) {

        showDialog();

        final String email = username.getText().toString();
        if (!isValidEmail(email)) {
            username.setError("Invalid Email");
            hideDialog();
        }else {

            //todo
        }

    }


    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
