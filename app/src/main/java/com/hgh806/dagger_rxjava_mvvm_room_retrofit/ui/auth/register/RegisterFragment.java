package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.register;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.R;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.utils.ViewModelProviderFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

public class RegisterFragment extends DaggerFragment {


//    @Inject
//    public ViewModelProviderFactory providerFactory;

    private RegisterViewModel viewModel;

    @BindView(R.id.email)
    EditText username;
    @BindView(R.id.codeVerify)
    EditText codeVerify;
    @BindView(R.id.password2)
    EditText password2;
    @BindView(R.id.btnVerify)
    Button btnVerify;
    @BindView(R.id.btn_sign)
    LinearLayout btnSign;
    @BindView(R.id.ln1)
    LinearLayout ln1;
    private ProgressDialog pDialog;

    private Unbinder unbinder;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this, view);

//        viewModel = new ViewModelProvider(this, providerFactory).get(RegisterViewModel.class);



        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);

        FlowingGradientClass grad = new FlowingGradientClass();
        grad.setBackgroundResource(R.drawable.transreg)
                .onLinearLayout(ln1)
                .setTransitionDuration(2000)
                .start();
    }

    public void checkRegister(View view) {

        final String email = username.getText().toString();
        if (!isValidEmail(email)) {
            //Set error message for email field
            username.setError("Invalid Email");
        }

        final String pass = codeVerify.getText().toString();
        if (!isValidPassword(pass)) {
            //Set error message for password field
            codeVerify.setError("Password cannot be empty");
        }

        final String pass2 = password2.getText().toString();
        if (!isValidPassword(pass2)) {
            //Set error message for password field
            password2.setError("Password cannot be empty");
        }

        if (!pass.equals(pass2)){
            password2.setError("Passwords doesn't match");
        }

        if(isValidEmail(email) && isValidPassword(pass)) {
            pDialog.setMessage("Registering ...");
            showDialog();
        }

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password
    private boolean isValidPassword(String pass) {
        return pass != null && pass.length() >= 4;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
