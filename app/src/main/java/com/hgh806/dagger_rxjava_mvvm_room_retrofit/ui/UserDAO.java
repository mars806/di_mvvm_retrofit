package com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface UserDAO {

    @Query("SELECT * FROM user")
    UserInfoModel.User get_user_info();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInfo(UserInfoModel.User... model);

    @Query("DELETE FROM user")
    void delete();

}
