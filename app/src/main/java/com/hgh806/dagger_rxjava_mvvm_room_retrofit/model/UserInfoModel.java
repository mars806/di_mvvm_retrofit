package com.hgh806.dagger_rxjava_mvvm_room_retrofit.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

public class UserInfoModel implements Serializable {

    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user")
    @Expose
    private User user;
    private final static long serialVersionUID = 5334312021521113953L;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



    @Entity(tableName = "user")
    public static class User implements Serializable
    {

        @SerializedName("name")
        @Expose
        @ColumnInfo(name = "name")
        private String name;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        @ColumnInfo(name = "email")
        private String email;
        @SerializedName("password")
        @Expose
        @ColumnInfo(name = "password")
        private String password;
        @SerializedName("avatar")
        @Expose
        @ColumnInfo(name = "avatar")
        private String avatar;
        @SerializedName("desc")
        @Expose
        @ColumnInfo(name = "desc")
        private String desc;
        @SerializedName("authKey")
        @Expose
        @ColumnInfo(name = "authKey")
        private String authKey;
        @SerializedName("authToken")
        @Expose
        @ColumnInfo(name = "authToken")
        private String authToken;
        @SerializedName("point")
        @Expose
        @ColumnInfo(name = "point")
        private int point;
        @SerializedName("inviteKey")
        @Expose
        @ColumnInfo(name = "inviteKey")
        private String inviteKey;
        @SerializedName("verified")
        @Expose
        @ColumnInfo(name = "verified")
        private boolean verified;
        @SerializedName("isActive")
        @Expose
        @ColumnInfo(name = "isActive")
        private boolean isActive;
        @SerializedName("_id")
        @Expose
        @PrimaryKey
        @NonNull
        private String id;
        @SerializedName("__v")
        @Expose
        @ColumnInfo(name = "__v")
        private int v;
        private final static long serialVersionUID = -9020530912036024303L;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getAuthKey() {
            return authKey;
        }

        public void setAuthKey(String authKey) {
            this.authKey = authKey;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }

        public String getInviteKey() {
            return inviteKey;
        }

        public void setInviteKey(String inviteKey) {
            this.inviteKey = inviteKey;
        }

        public boolean isVerified() {
            return verified;
        }

        public void setVerified(boolean verified) {
            this.verified = verified;
        }

        public boolean isIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getV() {
            return v;
        }

        public void setV(int v) {
            this.v = v;
        }

    }
}
