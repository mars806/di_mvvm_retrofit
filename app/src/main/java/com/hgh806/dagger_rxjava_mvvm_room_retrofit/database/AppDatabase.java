package com.hgh806.dagger_rxjava_mvvm_room_retrofit.database;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.UserDAO;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {UserInfoModel.User.class}, version = 4, exportSchema = true)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDAO userDAO();

}