package com.hgh806.dagger_rxjava_mvvm_room_retrofit;

import android.util.Log;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.database.AppDatabase;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.AuthResource;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;


@Singleton
public class SessionManager {
    private static final String TAG = "DaggerExample";

    private UserInfoModel.User user;
    private AppDatabase database;

    // data
    private MediatorLiveData<AuthResource<UserInfoModel>> cachedUser = new MediatorLiveData<>();

    @Inject
    public SessionManager(UserInfoModel.User user, AppDatabase database) {
        this.user = user;
        this.database = database;
    }

    public boolean isLoggedIn(){
        return user.getAuthKey() != null;
    }

    public void authenticateWithId(final LiveData<AuthResource<UserInfoModel>> source) {
        if(cachedUser != null){
            cachedUser.setValue(AuthResource.loading(null));
            cachedUser.addSource(source, userAuthResource -> {
                cachedUser.setValue(userAuthResource);
                cachedUser.removeSource(source);
                if(userAuthResource.status.equals(AuthResource.AuthStatus.ERROR)){
                    cachedUser.setValue(AuthResource.logout());
                }
                if (userAuthResource.status.equals(AuthResource.AuthStatus.AUTHENTICATED)){
                    addToDataBase(userAuthResource.data.getUser());
                }
            });
        }
    }

    private void addToDataBase(UserInfoModel.User model) {
        database.userDAO().insertInfo(model);
    }

    public void logOut() {
        Log.d(TAG, "logOut: logging out...");
        cachedUser.setValue(AuthResource.logout());
    }


    public LiveData<AuthResource<UserInfoModel>> getAuthUser(){
        return cachedUser;
    }

}

