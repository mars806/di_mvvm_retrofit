package com.hgh806.dagger_rxjava_mvvm_room_retrofit;



import com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class BaseApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();

//        return null;
    }
}
