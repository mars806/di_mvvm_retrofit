package com.hgh806.dagger_rxjava_mvvm_room_retrofit.network;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.utils.Config;

import io.reactivex.Flowable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AuthApi {

    @FormUrlEncoded
    @POST(Config.LOGIN)
    Flowable<UserInfoModel> login(@Field("email") String email, @Field("password") String password);

}
