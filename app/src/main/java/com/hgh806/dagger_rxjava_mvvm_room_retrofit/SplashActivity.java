package com.hgh806.dagger_rxjava_mvvm_room_retrofit;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import dagger.android.support.DaggerAppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.AuthActivity;

import javax.inject.Inject;

public class SplashActivity extends DaggerAppCompatActivity {

    @Inject
    SessionManager sessionManager;

    private final Handler mHideHandler = new Handler();

    private final Runnable mHideRunnable = this::hide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        new Handler().postDelayed(() -> {

            // Check if user is already logged in or not
            if (sessionManager.isLoggedIn()) {
                // User is already logged in. Take him to main activity
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }else {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this, AuthActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                finish();
            }
        }, 1000);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide();
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    private void delayedHide() {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, 500);
    }

}
