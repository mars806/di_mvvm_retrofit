package com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.auth;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.ViewModelKey;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.forgotPassword.ForgotPassViewModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.login.LoginViewModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.register.RegisterViewModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.verify.VerifyViewModel;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class AuthViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPassViewModel.class)
    public abstract ViewModel bindForgotPassViewModel(ForgotPassViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel.class)
    public abstract ViewModel bindForgotPassViewModelRegisterViewModel(RegisterViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    public abstract ViewModel bindLoginViewModel(LoginViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(VerifyViewModel.class)
    public abstract ViewModel bindVerifyViewModel(VerifyViewModel viewModel);
}
