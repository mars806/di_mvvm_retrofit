package com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.auth;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.login.LoginFragment;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.register.RegisterFragment;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.auth.verify.VerifyFragment;
import com.sirvar.robin.ForgotPasswordFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AuthFragmentBuilderModule {

    @ContributesAndroidInjector
    abstract LoginFragment contributeLoginFragment();

    @ContributesAndroidInjector
    abstract RegisterFragment contributeRegisterFragment();

    @ContributesAndroidInjector
    abstract ForgotPasswordFragment contributeForgotPasswordFragment();

    @ContributesAndroidInjector
    abstract VerifyFragment contributeVerifyFragment();
}
