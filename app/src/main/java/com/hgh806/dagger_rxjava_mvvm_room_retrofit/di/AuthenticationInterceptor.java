package com.hgh806.dagger_rxjava_mvvm_room_retrofit.di;


import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.utils.Config;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@Singleton
public class AuthenticationInterceptor implements Interceptor {
    private String authToken;
    private String key;

    @Inject
    public AuthenticationInterceptor(UserInfoModel.User userInfoModel) {
        this.authToken = userInfoModel.getAuthToken();
        this.key = userInfoModel.getAuthKey();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .addHeader(Config.API_KEY, key)
                .addHeader(Config.API_Token, authToken);
        Request request = builder.build();
        Response mainResponse = chain.proceed(request);

//        if (session.isLoggedIn()) {
//            // if response code is 401 or 403, 'mainRequest' has encountered authentication error
//            if (mainResponse.code() == 401 || mainResponse.code() == 403) {
//                session.invalidate();
//            }
//        }

        return mainResponse;
    }
}