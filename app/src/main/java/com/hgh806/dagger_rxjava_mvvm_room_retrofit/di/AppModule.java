package com.hgh806.dagger_rxjava_mvvm_room_retrofit.di;

import android.app.Application;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.database.AppDatabase;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.database.DatabaseClient;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.utils.Config;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    @Singleton
    @Provides
    AppDatabase provideDatabase(Application application){
        return DatabaseClient.getInstance(application).getAppDatabase();
    }

    @Singleton
    @Provides
    UserInfoModel.User provideUserInfoModel(AppDatabase database) {
        UserInfoModel.User user = database.userDAO().get_user_info();
        if (user != null) {
            return user;
        } else {
            return new UserInfoModel.User();
        }
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    @Singleton
    AuthenticationInterceptor authenticationInterceptor(UserInfoModel.User userInfoModel){
        return new AuthenticationInterceptor(userInfoModel);
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClint(AuthenticationInterceptor authenticationInterceptor,
                                    HttpLoggingInterceptor httpLoggingInterceptor){
        return new OkHttpClient().newBuilder()
                .addInterceptor(authenticationInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }


    @Provides
    @Singleton
    @Named("base_retrofit")
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Config.BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @Named("login")
    Retrofit provideRetrofit2() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Config.BASE_URL)
                .build();
    }
}


