package com.hgh806.dagger_rxjava_mvvm_room_retrofit.di;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.MainActivity;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.SplashActivity;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.auth.AuthFragmentBuilderModule;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.auth.AuthModule;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.auth.AuthScope;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.auth.AuthViewModelsModule;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.ui.AuthActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {


    @ContributesAndroidInjector
    abstract SplashActivity contributeSplashActivity();

    @AuthScope
    @ContributesAndroidInjector(
            modules = {AuthFragmentBuilderModule.class, AuthViewModelsModule.class, AuthModule.class})
    abstract AuthActivity contributeAuthActivity();


  //  @MainScope
    @ContributesAndroidInjector(
         //   modules = {MainFragmentBuildersModule.class, MainViewModelsModule.class, MainModule.class}
    )
    abstract MainActivity contributeMainActivity();


}
