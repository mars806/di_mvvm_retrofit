package com.hgh806.dagger_rxjava_mvvm_room_retrofit.di.auth;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.model.UserInfoModel;
import com.hgh806.dagger_rxjava_mvvm_room_retrofit.network.AuthApi;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AuthModule {

    @AuthScope
    @Provides
    @Named("auth_user")
    static UserInfoModel someUser(){
        return new UserInfoModel();
    }

    @AuthScope
    @Provides
    static AuthApi provideAuthApi(@Named("login") Retrofit retrofit){
        return retrofit.create(AuthApi.class);
    }

}
