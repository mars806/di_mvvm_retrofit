package com.hgh806.dagger_rxjava_mvvm_room_retrofit.di;

import com.hgh806.dagger_rxjava_mvvm_room_retrofit.utils.ViewModelProviderFactory;

import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelFactoryModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory viewModelFactory);

}

